// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import MuseUI from 'muse-ui'
import 'muse-ui/dist/muse-ui.css'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
var infiniteScroll =  require('vue-infinite-scroll')
import VueMoment from 'vue-moment'
import VueCookie from 'vue-cookie'
import VueFriendlyIframe from 'vue-friendly-iframe';

Vue.use(VueFriendlyIframe);
Vue.use(VueCookie )
Vue.use(VueMoment)
Vue.use(infiniteScroll)
Vue.use(BootstrapVue)
Vue.use(MuseUI)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
