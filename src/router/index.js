import Vue from 'vue'
import Router from 'vue-router'
import LayoutApp from '@/pages/layout/LayoutApp'
import HomePage from '@/pages/HomePage'
import EventPage from '@/pages/EventPage'
import FeedPage from '@/pages/FeedPage'
import RewardsPage from '@/pages/RewardsPage'
import ProfilePage from '@/pages/ProfilePage'
import DetailRewards from '@/pages/DetailRewards'
import BuktiTransaksi from '@/pages/BuktiTransaksi'
import HotPromo from '@/pages/HotPromo'
import LoginPage from '@/pages/LoginPage'
import Leaderboard from '@/pages/Leaderboard'
import PromotionDetail from '@/pages/PromotionDetail'
import KatalogPage from '@/pages/KatalogPage'
import RedemtionPage from '@/pages/RedemtionPage'
import SignUp from '@/pages/SignUp'
import ProfileEdit from '@/pages/ProfileEdit'
import PhotoUpload from '@/pages/PhotoUpload'
import LayoutLogin from '@/pages/LayoutLogin'
import PasswordForgot from '@/pages/PasswordForgot'
import TermService from '@/pages/TermService'
import DetailEvent from '@/pages/DetailEvent'
import ComercePage from '@/pages/ComercePage'
import DataNegara from '@/pages/DataNegara'
import CovidCari from '@/pages/CovidCari'

Vue.use(Router)

export default new Router({
  routes: [
   
    {
      path: '/',
      name: 'LayoutApp',
      component: LayoutApp,
      redirect: '/home',
      children: [
        {
          path: 'home',
          name: 'home',
          component: HomePage
        },
        {
          path: 'negara',
          name: 'negara',
          component: DataNegara
        },
        {
          path: 'cari',
          name: 'cari',
          component: CovidCari
        },
        
        {
          path: 'berita',
          name: 'berita',
          component: FeedPage
        },
        
      ]
    } 
  ]
})
