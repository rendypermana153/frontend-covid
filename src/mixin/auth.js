export const auth = {
    data () {
        return {
            token: this.$cookie.get('token_ebp')
        }
    },
    methods: {
        authentication () {
            if(!this.token || this.token != ''){
                this.$router.push({'name': 'home'})
            }
        }
    },
    mounted () {
        if(!this.token || this.token === ''){
            this.$router.push({'name': 'login'})
        }
    }
}